-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  mar. 22 juin 2021 à 09:30
-- Version du serveur :  10.1.26-MariaDB
-- Version de PHP :  7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `moniteurs`
--

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `sexe` varchar(255) NOT NULL,
  `date_naissance` date NOT NULL,
  `num_phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `profil` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `nom`, `prenom`, `sexe`, `date_naissance`, `num_phone`, `email`, `password`, `country`, `profil`) VALUES
(12, 'Berger', 'Elouti', 'Homme', '2003-01-11', '697353571', 'bergereloutioumbechester@yahoo.com', 'bergerelouti', 'portugal', 'Capture1.JPG'),
(19, 'Nagam', 'Tricia', 'Femme', '2021-06-21', '67857989857', 'oumbepascalinekj@gmail.com', 'dhkll;knj', 'cameroun', 'Capture1.JPG'),
(21, 'Nagam', 'bonjour', 'Femme', '2021-06-21', '67857989857', 'bonjourmgcmon@gmail.com', 'sdfghjkhklj', 'cameroun', 'Capture1.JPG'),
(28, 'heko', 'kali', 'Femme', '2021-06-21', '56758797', 'bonjout4whsdrmgcmon@gmail.com', '09876tjklkjh', 'algerie', ''),
(31, 'Degrace', 'dgrrddsds', 'Homme', '2021-06-22', '33456654333', 'frer@gmail.com', 'rrer445f', 'italie', '');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
