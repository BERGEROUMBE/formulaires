<!DOCTYPE html>
<html>
<head>
	<title>	</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

  <style type="text/css">
    h1{
      color: #00B59C; 
      padding:20px 0px 50px 0px;
      font-size: 50px;
    }
    div{
      text-align: center!important;
    }
    body{
       background: rgba(0,181,156,.1)!important;
    }
  </style>

</head>
<body>
  <div class="container-fluid"  > 
    <h1 >Registrant informations</h1>
  	<?php 
          $bdd= new PDO('mysql:host=localhost;dbname=moniteurs','root','',array(PDO::ATTR_ERRMODE =>PDO::ERRMODE_EXCEPTION));


              $nom = strip_tags($_POST[ 'nom' ]);
              $prenom = strip_tags($_POST[ 'prenom' ]);
              $sexe = strip_tags($_POST['sexe']);
              $date= strip_tags($_POST[ 'date_naissance' ]);
              $telephone = strip_tags($_POST[ 'num_phone' ]);
              $email = strip_tags($_POST[ 'email' ]);
              $password = strip_tags($_POST[ 'password' ]);
              $country = strip_tags($_POST[ 'country' ]);
              $photo = ($_FILES[ 'profil' ]);
              
              if(isset($_POST)){
                  if (isset($_FILES['profil']) AND $_FILES['profil']['error'] == 0)
                  {
                      if ($_FILES['profil']['size'] <= 3000000)
                      {
                        $infosfichier = pathinfo($_FILES['profil']['name']);
                        $extension_upload = $infosfichier['extension'];
                        $extensions_autorisees = array('jpg', 'jpeg', 'png','JPEG','PNG','JPG');
                        if (in_array($extension_upload, $extensions_autorisees))
                        {
                          move_uploaded_file($_FILES['profil']['tmp_name'], '../images/' . basename($_FILES['profil']['name']));
                          $p = $_FILES['profil']['name'];
                          $Image="<img style='height:70px; width:70px;' src='../images/".$p." ' >"."<br>";
                        }
                        else
                        {
                            echo 'extention non-autorisee';
                        }
                      }
                      else
                      {
                          echo 'Image trop Volumineuse';
                      }
                  }
              }
                  
          
              $dataInsert=$bdd->prepare('INSERT into user(nom ,prenom ,sexe ,date_naissance, num_phone,
  	 	 email,password,country,profil)
                   VALUES(?,?,?,?,?,?,?,?,?)');
               $dataInsert->execute(array($nom,
                                          $prenom,
                                          $sexe,
                                          $date,
                                          $telephone,
                                          $email,
                                          $password,
                                          $country,
                                          $_FILES['profil']['name']));
                 
    ?>

    	<div class="table-responsive-md table-responsive-sm table-responsive-lg ">
              <table class="table table-bordered table-striped table-hover">
  	              <thead class="thead-light">
  	                <tr >
  	                  <th scope="col">N<sup>o</sup></th>
  	                  <th scope="col">Name</th>
  	                  <th scope="col">Last Name</th>
  	                  <th scope="col">sexe</th>
  	                  <th scope="col" class="disp1">Profil</th>
  	                  <th scope="col">Birth's years</th>
  	                  <th scope="col">Phone</th>
  	                  <th scope="col">E-mail</th>
  	                  <th scope="col">Country</th>
  	                </tr>
  	              </thead>
  	              <tbody>
  	                <?php 
  	                    $response = $bdd->query('SELECT  * FROM user');
  	                            $i=1;
  	                            while ($donnees=$response->fetch()) {

  	                                  echo '
  	                                  <tr style="text-align:center;">
  	                                    <td>'.$i.'</td> 
  	                                    <td>'.$donnees['nom'].'</td> 
  	                                    <td>'.$donnees['prenom'].'</td>
  	                                    <td>'.$donnees['sexe'].'</td> 
  	                                    <td class="disp1"> <img src="../images/'.$donnees['profil'].'"; style=" width: 80px; height: 80px;border-radius: 50%;"</td>
  	                                    <td>'.$donnees['date_naissance'].'</td> 
  	                                    <td>'.$donnees['num_phone'].'</td> 
  	                                    <td>'.$donnees['email'].'</td>
  	                                    <td>'.$donnees['country'].'</td>
  	                                    </tr>';
  	                                  $i++;
  	                            }


  	                 ?>

  	         	 </tbody>
           	</table>
      </div>
  </div>     	
</body>
</html>
