<!DOCTYPE html>
<html class="translated-ltr">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<title>Envoie des données</title>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">

<!-- appel des feuilles de style -->
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet">	
</head>

<body>

	<div class="container col-md-5 col-sm-5 col-xs-5 ">
		<div class="title">
			<h1>Sign In</h1>
		</div>
		
		<form method="post" action="pages/traitement.php" enctype="multipart/form-data">
			
			<div class="form-group">
				<label ><span class="glyphicon glyphicon-user">-Name</span></label>
				<input class="form-control" type="text" name="nom"  placeholder="Entrer your Name" required="" autofocus="" 	 maxlength="20">
			</div>

			<div class="form-group">
				<label ><span class="glyphicon glyphicon-user">-Last-Name</span></label>
				<input class="form-control" type="text" name="prenom"  placeholder="Entrer your First name" required="" maxlength="20">
			</div>

			<div class="form-group">
				<label><span class="glyphicon glyphicon-user">-Gender</span></label>
				<select name="sexe" class="sexe form-control" value="">
                    <option value="Homme">Male</option>
                    <option value="Femme">Female	</option>
                </select>
			</div>

			<div class="form-group">
				<label > <span class="glyphicon glyphicon-calendar">-Birth's-Year</span></label>
				<input class="form-control" type="date" name="date_naissance"  placeholder="Entrer your date of birth " required="">
			</div>

			<div class="form-group">
				<label ><span class="glyphicon glyphicon-phone">-Phone</span></label>
				<input class="form-control" type="number" min="1" name="num_phone"  placeholder="Entrer your phone number" required="" >
			</div>

			<div class="form-group">
				<label > <span class="glyphicon glyphicon-envelope">-E-mail</span></label>
				<input class="form-control" type="email" name="email" placeholder="Entrer your email" required="">
			</div>

			<div class="form-group">
				<label > <span class="glyphicon glyphicon-pushpin">-Password</span></label>
				<input class="form-control" type="password" name="password" placeholder="Entrer your Password" required="">
			</div>

			<div class="form-group">
				<label > <span class="glyphicon glyphicon-globe">-Country</span></label>
				<select class="form-control" name="country" required="">
					<optgroup label="Afrique">
						<option value="cameroun" selected="">Cameroun</option>
						<option value="congo">Congo</option>
						<option value="algerie">Algérie</option>
						<option value="egypte">Egypte</option>
						<option value="maroc">Maroc</option>
						<option value="gabon">Gabon</option>
					</optgroup>
		           <optgroup label="Europe">
		               <option value="france">France</option>
		               <option value="espagne">Espagne</option>
		               <option value="italie">Italie</option>
		               <option value="royaume-uni">Royaume-Uni</option>
		               <option value="portugal">Portugal</option>
		               <option value="portugal">Allemagne</option>
		           </optgroup>
		           <optgroup label="Amérique">
		               <option value="canada">Canada</option>
		               <option value="etats-unis">Etats-Unis</option>
		               <option value="etats-unis">Brazil</option>
		               <option value="etats-unis">Argentine</option>
		               <option value="etats-unis">Colombie</option>
		               <option value="etats-unis">Venezuela</option>
		           </optgroup>
		           <optgroup label="Asie">
		               <option value="chine">Chine</option>
		               <option value="japon">Japon</option>
		               <option value="japon">Indonesie</option>
		               <option value="japon">Coré du sud</option>
		               <option value="japon">Inde</option>
		           </optgroup>
      			</select>
			</div>
			<div class="form-group">
				<label > <span class="glyphicon glyphicon-picture">-Profil</span></label>
				<input class="form-control" type="file" id="photo" name="profil" accept="image/*" onchange="loadFile(event)">
				<div class="div_img">
	               <img id="foto" >
	          	</div>
			</div>

			<div class="form-group reset_send">
				<input class="btn cancel" type="reset" value="Cancel">
				<input class="btn pull pull-right register " type="submit" value="Send">
			</div>
		</form>

	</div>


<!-- appel du javascript -->

<script type="text/javascript">

	var loadFile = function(event) {
    var profil = document.getElementById('foto');
    profil.src = URL.createObjectURL(event.target.files[0]);
};

</script>
<!-- <script src="bootstrap-3.2.0-dist/js/jquery-1.10.2.js"></script> 
<script src="bootstrap-3.2.0-dist/js/bootstrap.js"></script>  -->	
<script type="text/javascript" src="javascript/script.js"></script>	
</body>
</html>